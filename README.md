# sensibill

> Run using `npm start`

## About

This project uses [Feathers](http://feathersjs.com). An open source web framework for building modern real-time applications.

## Getting Started

Getting up and running is as easy as 1, 2, 3.

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed. You will also need an instance of Mongo running.
2. Install your dependencies

    ```
    cd path/to/sensibill; npm install
    ```

3. Start your app

    ```
    npm start
    ```

4. Operation

    All models - users, receipts, locations - support JSON, RESTful CRUD vis-a-via GET model (gets all), get model/id, post model, put model/id, patch model/id, and delete model/id.
    For example, 
    GET receipts/
    will get all receipts while
    GET receipts/1
    will get the receipt with id=1.
    
    The use of patch is preferred over put, as put is essentially a replacement - you have to include all of the fields. Patch is a true update.

    App uses the following models:

    -users (contains a bank field)

    -receipts

    -locations (contains merchant info)

    No bank model was implemented as the app is intended to be run as a separate instance for each bank. Why? White label solution. We don't have users, the bank does, and I think we should keep separate banks' data separate and self contained. 

    Query parameters are translated into 'WHERE' clauses. For example,
    GET /users?bank='Scotiabank'
    will return all users belonging to Scotiabank.


    To query an inequality, use [$gt], [$gte], [$lt], [$lte] in the query parameters. Using the following format, eg. for receipts over $500:
    GET /receipts?total[$gt]=500
