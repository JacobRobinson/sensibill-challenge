// locations-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const locations = new mongooseClient.Schema({
    merchantName: { type: String, required: true },
    address: { type: String, required: true },
  }, {
    timestamps: true
  });

  return mongooseClient.model('locations', locations);
};
