// receipts-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const mongoose = require('mongoose');
  const receipts = new mongooseClient.Schema({
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'users', required: true, index: true },
    merchantInfo: { type: mongoose.Schema.Types.ObjectId, ref: 'locations', required: true, index: true },
    date: { type: Date, required: true },
    lineItems: [{ type: mongoose.Schema.Types.Mixed, required: true }],
    total: { type: Number, required: true },
  }, {
    timestamps: true
  });

  return mongooseClient.model('receipts', receipts);
};
